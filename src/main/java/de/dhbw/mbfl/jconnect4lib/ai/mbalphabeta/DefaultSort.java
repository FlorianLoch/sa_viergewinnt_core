/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dhbw.mbfl.jconnect4lib.ai.mbalphabeta;

import de.dhbw.mbfl.jconnect4lib.board.Position;

/**
 *
 * @author Maurice Busch <busch.maurice@gmx.net>
 */
public class DefaultSort implements IAlphaBetaSort
{
    /**
     * This Method sorts all posible Turns so that the AlphaBeta Algorithm can cut off very
     * fast
     * @param posibleTurns
     * @return 
     */
    @Override
    public Position[] sortPosibleTurns(Position[] posibleTurns)
    {
        return posibleTurns;
    }
}
