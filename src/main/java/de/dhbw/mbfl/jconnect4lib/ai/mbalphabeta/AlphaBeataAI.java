/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dhbw.mbfl.jconnect4lib.ai.mbalphabeta;

import de.dhbw.mbfl.jconnect4lib.ai.AI;
import de.dhbw.mbfl.jconnect4lib.board.Board;
import de.dhbw.mbfl.jconnect4lib.board.Position;
import de.dhbw.mbfl.jconnect4lib.board.Stone;

/**
 *
 * @author Maurice Busch <busch.maurice@gmx.net>
 */
public class AlphaBeataAI implements AI
{

    private final IAlpahBetaDeep deep;
    private final IAlphaBetaSort sort;
    private final IAlphaBetaEvaluate evaluate;

    private Board board;
    private Stone stoneAI;
    private Stone stonePlayer;
    private int startDeep;

    private Position bestTurn;
    
    public AlphaBeataAI(IAlpahBetaDeep deep, IAlphaBetaSort sort, IAlphaBetaEvaluate evaluate)
    {
        this.deep = deep;
        this.sort = sort;
        this.evaluate = evaluate;
    }

    @Override
    public Position calculateTurn(Board board, Stone stoneAI)
    {
        this.board = board;
        this.stoneAI = stoneAI;
        this.stonePlayer = stoneAI.getOtherStone();
        this.bestTurn = null;

        this.startDeep = this.deep.calculateDeep(board);

        int value = max(this.startDeep, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return this.bestTurn;
    }

    private int max(int deep, int alpha, int beta)
    {
        if(deep <= 0 || !board.isGameRunning()) return evaluate.evaluate(board);

        int maxValue = alpha;
        Position[] posibleTurns = sort.sortPosibleTurns(board.getPosibleTurns());

        for(Position pos : posibleTurns)
        {
            if(pos != null)
            {
                board.addStone(pos, stoneAI);
                int value = min(deep -1, maxValue, beta);
                board.undoLastTurn();
                if(value > maxValue)
                {
                    maxValue = value;
                    if(maxValue >= beta) break;

                    if(deep == startDeep) bestTurn = pos;
                }
            }
        }

        return maxValue;
    }

    private int min(int deep, int alpha, int beta)
    {
        if(deep <= 0 || !board.isGameRunning()) return evaluate.evaluate(board) * -1;

        int minValue = beta;
        Position[] posibleTurns = sort.sortPosibleTurns(board.getPosibleTurns());

        for(Position pos : posibleTurns)
        {
            if(pos != null)
            {
                board.addStone(pos, stonePlayer);
                int value = max(deep -1, alpha, minValue);
                board.undoLastTurn();

                if(value < minValue)
                {
                    minValue = value;
                    if(minValue <= alpha) break;
                }
            }
        }

        return minValue;
    }

}
