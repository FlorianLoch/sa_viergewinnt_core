/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dhbw.mbfl.jconnect4lib.ai.mbalphabeta;

import de.dhbw.mbfl.jconnect4lib.board.Board;

/**
 *
 * @author Maurice Busch <busch.maurice@gmx.net>
 */
public interface IAlpahBetaDeep
{
    int calculateDeep(Board board);
}
