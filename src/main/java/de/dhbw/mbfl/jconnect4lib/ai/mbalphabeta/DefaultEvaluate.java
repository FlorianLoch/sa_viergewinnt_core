/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dhbw.mbfl.jconnect4lib.ai.mbalphabeta;

import de.dhbw.mbfl.jconnect4lib.board.Board;
import de.dhbw.mbfl.jconnect4lib.board.Position;
import de.dhbw.mbfl.jconnect4lib.board.Size;
import de.dhbw.mbfl.jconnect4lib.board.Stone;

/**
 *
 * @author Maurice Busch <busch.maurice@gmx.net>
 */
public class DefaultEvaluate implements IAlphaBetaEvaluate
{

    private static final int[][] evaluationTable =
    {
        {
            3, 4, 5, 7, 5, 4, 3
        },
        {
            4, 6, 8, 10, 8, 6, 4
        },
        {
            5, 8, 11, 13, 11, 8, 5
        },
        {
            5, 8, 11, 13, 11, 8, 5
        },
        {
            4, 6, 8, 10, 8, 6, 4
        },
        {
            5, 7, 9, 10, 9, 7, 5
        }
    };

    private final Stone stoneAI;
    private final Stone stonePlayer;

    public DefaultEvaluate(Stone stoneAI, Stone stonePlayer)
    {
        this.stoneAI = stoneAI;
        this.stonePlayer = stonePlayer;
    }

    /**
     * bigger 0 if AI is likely to win has the most strategic places based on
     * the utility function = 0 if equally lower 0 if the player is likely to
     * win.
     *
     * @param board
     *
     * @return
     */
    @Override
    public int evaluate(Board board)
    {
        if(Size.BOARD.column() != evaluationTable[0].length || Size.BOARD.row() != evaluationTable.length)
        {
            throw new RuntimeException("The evaluation Function can only evaluate a Board with a spezial size.");
        }
        int sum = 0;
        for(int row = 0; row < Size.BOARD.row(); row++)
        {
            for(int col = 0; col < Size.BOARD.column(); col++)
            {
                Position pos = new Position(col, row);
                Stone stone = board.getStone(pos);

                if(stone == stoneAI)
                {
                    sum += evaluationTable[row][col];
                }
                else if(stone == stonePlayer)
                {
                    sum -= evaluationTable[row][col];
                }
            }
        }
        return sum;
    }
}
